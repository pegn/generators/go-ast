# Generators written in Go

This repository contains various experimental generators.
These generators all have their corresponding [branches](https://gitlab.com/pegn/generators/go-ast/-/branches).

List of (unfinished) generators based on:
- [bytes](https://gitlab.com/pegn/generators/go-ast/-/tree/bytes)
- [templates](https://gitlab.com/pegn/generators/go-ast/-/tree/templates)

